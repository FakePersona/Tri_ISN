#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass beamer
\begin_preamble
\usetheme{Warsaw}
% or ...

\setbeamercovered{transparent}
% or whatever (possibly just delete it)
\end_preamble
\use_default_options false
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman times
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 2
\use_package amssymb 2
\use_package cancel 0
\use_package esint 0
\use_package mathdots 1
\use_package mathtools 0
\use_package mhchem 1
\use_package stackrel 0
\use_package stmaryrd 0
\use_package undertilde 0
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 2
\tocdepth 2
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
This file is a solution template for:
\end_layout

\begin_layout Itemize
Talk at a conference/colloquium.
 
\end_layout

\begin_layout Itemize
Talk length is about 20min.
 
\end_layout

\begin_layout Itemize
Style is ornate.
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
 
\end_layout

\begin_layout Plain Layout
In principle, this file can be redistributed and/or modified under the terms
 of the GNU Public License, version 2.
 However, this file is supposed to be a template to be modified for your
 own needs.
 For this reason, if you use this file as a template and not specifically
 distribute it as part of a another package/program, the author grants the
 extra permission to freely copy and modify this file as you see fit and
 even to delete this copyright notice.
 
\end_layout

\end_inset


\end_layout

\begin_layout Title
Tris et complexité
\begin_inset Argument 1
status open

\begin_layout Plain Layout
Tris et complexité
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
optional, use only with long paper titles
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subtitle
De l'importance de savoir trier efficacement
\end_layout

\begin_layout Author
S.
\begin_inset space ~
\end_inset

Rémy 
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
and
\end_layout

\end_inset

 T.
\begin_inset space ~
\end_inset

Bastien
\begin_inset Note Note
status collapsed

\begin_layout Itemize
Give the names in the same order as the appear in the paper.
 
\end_layout

\begin_layout Itemize
Use the 
\begin_inset Quotes eld
\end_inset

Institute mark
\begin_inset Quotes erd
\end_inset

 inset (
\family sans
Insert\SpecialChar \menuseparator
Custom Insets\SpecialChar \menuseparator
InstituteMark
\family default
) only if the authors have different affiliations.
\end_layout

\end_inset


\begin_inset Argument 1
status open

\begin_layout Plain Layout
Sun, Thomas
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
- optional, use only with lots of authors
\end_layout

\begin_layout Plain Layout
- if there are really lots of authors, use 
\begin_inset Quotes eld
\end_inset

Author et al.
\begin_inset Quotes erd
\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Institute
Département d'informatique
\begin_inset Newline newline
\end_inset

Ecole Normale Supérieure de Rennes
\begin_inset Note Note
status collapsed

\begin_layout Itemize
Use the 
\begin_inset Quotes eld
\end_inset

Institute mark
\begin_inset Quotes erd
\end_inset

 inset (
\family sans
Insert\SpecialChar \menuseparator
Custom Insets\SpecialChar \menuseparator
InstituteMark
\family default
) only if there are several affiliations.
\end_layout

\begin_layout Itemize
Keep it simple, no one is interested in your street address.
\end_layout

\end_inset


\begin_inset Argument 1
status open

\begin_layout Plain Layout
Ecole Normale Supérieure de Rennes
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
optional, but mostly needed
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Date
Conférence au lycée Anita Conti, 10 Mars 2016 
\begin_inset Note Note
status collapsed

\begin_layout Itemize
Either use conference name or its abbreviation.
 
\end_layout

\begin_layout Itemize
Not really informative to the audience, more for people (including yourself)
 who are reading the slides online
\end_layout

\end_inset


\begin_inset Argument 1
status collapsed

\begin_layout Plain Layout
Conti 2016
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
optional, should be abbreviation of conference name
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
If you have a file called "institution-logo-filename.xxx", where xxx is a
 graphic format that can be processed by latex or pdflatex, resp., then you
 can add a logo by uncommenting the following:
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
pgfdeclareimage[height=0.5cm]{institution-logo}{ENS-Rennes.png}
\end_layout

\begin_layout Plain Layout


\backslash
logo{
\backslash
pgfuseimage{institution-logo}}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
The following causes the table of contents to be shown at the beginning
 of every subsection.
 Delete this, if you do not want it.
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
AtBeginSubsection[]{%
\end_layout

\begin_layout Plain Layout

  
\backslash
frame<beamer>{ 
\end_layout

\begin_layout Plain Layout

    
\backslash
frametitle{Outline}   
\end_layout

\begin_layout Plain Layout

    
\backslash
tableofcontents[currentsection,currentsubsection] 
\end_layout

\begin_layout Plain Layout

  }
\end_layout

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
If you wish to uncover everything in a step-wise fashion, uncomment the
 following command:
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
beamerdefaultoverlayspecification{<+->}
\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Sommaire
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
Structuring a talk is a difficult task and the following structure may not
 be suitable.
 Here are some rules that apply for this solution: 
\end_layout

\begin_layout Itemize
Exactly two or three sections (other than the summary).
 
\end_layout

\begin_layout Itemize
At *most* three subsections per section.
 
\end_layout

\begin_layout Itemize
Talk about 30s to 2min per frame.
 So there should be between about 15 and 30 frames, all told.
\end_layout

\begin_layout Itemize
A conference audience is likely to know very little of what you are going
 to talk about.
 So *simplify*! 
\end_layout

\begin_layout Itemize
In a 20min talk, getting the main ideas across is hard enough.
 Leave out details, even if it means being less precise than you think necessary.
 
\end_layout

\begin_layout Itemize
If you omit details that are vital to the proof/implementation, just say
 so once.
 Everybody will be happy with that.
 
\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout Section
Pourquoi trier ?
\end_layout

\begin_layout Subsection
Quelques problèmes basiques
\begin_inset Argument 2
status open

\begin_layout Plain Layout
Pourquoi trier ?
\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Tris d'éléments.
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Block
\begin_inset Argument 1
status open

\begin_layout Plain Layout
2-
\end_layout

\end_inset


\begin_inset Argument 2
status open

\begin_layout Plain Layout
Pourquoi trier?
\end_layout

\end_inset


\end_layout

\begin_layout Block
On veut déterminer un ordre de passage.
\end_layout

\begin_layout ExampleBlock
\begin_inset Argument 1
status open

\begin_layout Plain Layout
1-
\end_layout

\end_inset


\begin_inset Argument 2
status open

\begin_layout Plain Layout
Concrétement ?
\end_layout

\end_inset


\end_layout

\begin_layout ExampleBlock
APB utilise des algorithmes de tris pour gérer les voeux des élèves.
\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Recherche de plus petite distance.
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Block
\begin_inset Argument 1
status open

\begin_layout Plain Layout
2-
\end_layout

\end_inset


\begin_inset Argument 2
status open

\begin_layout Plain Layout
Pourquoi trier?
\end_layout

\end_inset


\end_layout

\begin_layout Block
Recherche de plus petite distance entre deux éléments d'un ensemble.
\end_layout

\begin_layout ExampleBlock
\begin_inset Argument 1
status open

\begin_layout Plain Layout
1-
\end_layout

\end_inset


\begin_inset Argument 2
status open

\begin_layout Plain Layout
Concrétement ?
\end_layout

\end_inset


\end_layout

\begin_layout ExampleBlock
On veut savoir quels avions sont les plus proches pour éviter un accident.
\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Mieux travailler.
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Block
\begin_inset Argument 1
status open

\begin_layout Plain Layout
2-
\end_layout

\end_inset


\begin_inset Argument 2
status open

\begin_layout Plain Layout
Pourquoi trier?
\end_layout

\end_inset


\end_layout

\begin_layout Block
Pouvoir travailler de maniére plus efficace.
\end_layout

\begin_layout ExampleBlock
\begin_inset Argument 1
status open

\begin_layout Plain Layout
1-
\end_layout

\end_inset


\begin_inset Argument 2
status open

\begin_layout Plain Layout
Concrétement ?
\end_layout

\end_inset


\end_layout

\begin_layout ExampleBlock
La dichotomie suppose qu'on travaille sur un ensemble trié.
\end_layout

\end_deeper
\begin_layout Subsection
Cahier des charges
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Durée
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Block
\begin_inset Argument 1
status open

\begin_layout Plain Layout
2-
\end_layout

\end_inset


\begin_inset Argument 2
status open

\begin_layout Plain Layout
Quelle contrainte ?
\end_layout

\end_inset


\end_layout

\begin_layout Block
Respecter des contraintes de temps raisonnables.
\end_layout

\begin_layout ExampleBlock
\begin_inset Argument 1
status open

\begin_layout Plain Layout
1-
\end_layout

\end_inset


\begin_inset Argument 2
status open

\begin_layout Plain Layout
Concrétement ?
\end_layout

\end_inset


\end_layout

\begin_layout ExampleBlock
Avant la fin du monde.
\end_layout

\end_deeper
\begin_layout Section
Quelques tris
\end_layout

\begin_layout Subsection
Tri bulle
\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Exemple 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename Bulle0.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Exemple 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename Bulle1.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Exemple 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename Bulle2.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Exemple 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename Bulle3.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Exemple 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename Bulle4.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Exemple 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename Bulle5.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Exemple 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename Bulle6.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Exemple 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename Bulle7.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Exemple 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename Bulle8.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Exemple 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename Bulle9.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Algorithme du tri bulle.
 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
On parcourt un tableau en comparant chaque élément avec son voisin de droite
\end_layout

\begin_layout Itemize
Si le voisin de droite est plus petit, on inverse les éléments et on continue.
\end_layout

\begin_layout Itemize
On continue jusqu'à l'avant dernier élément.
\end_layout

\begin_layout Itemize
En un passage, on est sûr que le plus grand élément est à droite.
\end_layout

\end_deeper
\begin_layout Subsection
Tri fusion
\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Fusion ? 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename fusion0.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Fusion ? 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename fusion1.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Fusion ? 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename fusion2.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Fusion ? 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename fusion3.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Fusion ? 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename fusion4.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Fusion ? 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename fusion5.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Fusion ? 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename fusion6.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Exemple 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename merge1.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Exemple 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename merge2.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Exemple 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename merge3.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Exemple 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename merge4.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Exemple 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename merge5.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Exemple 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename merge6.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Exemple 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename merge7.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Exemple 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename merge8.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Exemple 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename merge9.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Algorithme du tri fusion.
 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Si deux tableaux sont triés, il est facile de les fusionner.
\end_layout

\begin_layout Itemize
On divise le tableau en sous-tableau.
\end_layout

\begin_layout Itemize
Les tableaux unitaires sont triés.
\end_layout

\begin_layout Itemize
On procéde à la fusion.
\end_layout

\end_deeper
\begin_layout Frame

\end_layout

\begin_layout Section
Question de complexité
\end_layout

\begin_layout Subsection

\lang french
Notion de complexité
\end_layout

\begin_layout Frame

\lang french
\begin_inset Argument 4
status open

\begin_layout Plain Layout

\lang french
Comparer deux algorithmes ?
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize

\lang french
Première idée : mesurer le temps de calcul.
 
\end_layout

\begin_deeper
\begin_layout Itemize

\lang french
Problème : cette mesure dépendrait alors de l'ordinateur utilisé et non
 pas juste de l'algorithme.
 
\end_layout

\begin_layout Itemize

\lang french
Deuxième idée : compter les 
\shape italic
opérations élémentaires
\shape default
 effectuées.
 
\end_layout

\begin_layout Itemize

\lang french
Ici, on comptera par exemple le nombre de comparaisons effectuées.
 
\end_layout

\end_deeper
\end_deeper
\begin_layout Frame

\end_layout

\begin_layout Separator

\end_layout

\begin_layout Subsection
Tri bulle
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Complexité du tri bulle ?
\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Graphics
	filename Complexité_bulle1.png
	scale 80

\end_inset


\end_layout

\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Complexité du tri bulle ?
\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Graphics
	filename Complexité_bulle2.png
	scale 80

\end_inset


\end_layout

\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Complexité du tri bulle ?
\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Graphics
	filename Complexité_bulle3.png
	scale 80

\end_inset


\end_layout

\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Complexité du tri bulle ?
\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Graphics
	filename Complexité_bulle4.png
	scale 80

\end_inset


\end_layout

\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Complexité du tri bulle ?
\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Graphics
	filename Complexité_bulle5.png
	scale 80

\end_inset


\end_layout

\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Complexité du tri bulle ?
\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Graphics
	filename Complexité_bulle6.png
	scale 80

\end_inset


\end_layout

\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Complexité du tri bulle ?
\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Graphics
	filename Complexité_bulle7.png
	scale 80

\end_inset


\end_layout

\begin_layout Separator

\end_layout

\begin_layout Frame

\lang french
Il faut au plus 
\begin_inset Formula $\frac{n^{2}}{2}-\frac{n}{2}$
\end_inset

 comparaisons, le terme le plus important est 
\begin_inset Formula $n^{2}$
\end_inset

, on parle de 
\shape italic
complexité quadratique
\shape default
.
\end_layout

\begin_layout Frame
\begin_inset Graphics
	filename Graphe_quadratique.png
	width 7.5cm
	height 6cm

\end_inset


\end_layout

\begin_layout Separator

\end_layout

\begin_layout Subsection
Tri fusion
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Exemple 
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Graphics
	filename merge9.png
	width 10cm
	height 7cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame

\lang french
La complexité du tri fusion est en 
\begin_inset Formula $n\times ln(n)$
\end_inset

.
\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame

\lang french
\begin_inset Graphics
	filename Graphe_qlinéaire.png
	width 7.5cm
	height 6cm

\end_inset


\begin_inset Note Note
status collapsed

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Separator

\end_layout

\begin_layout Subsection

\lang french
Comparaison des complexités.
\end_layout

\begin_layout Frame

\lang french
La complexité du tri bulle dépasse vite celle du tri fusion.
\end_layout

\begin_layout Frame

\lang french
\begin_inset Graphics
	filename Comparaison.png
	width 9cm
	height 6cm

\end_inset


\end_layout

\begin_layout Separator

\end_layout

\begin_layout Section*
Conclusion
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Conclusion
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Tous les tris ne prennent pas
\color none
 
\color inherit

\begin_inset Flex Alert
status open

\begin_layout Plain Layout
le même temps
\end_layout

\end_inset


\color none
.
\end_layout

\begin_layout Itemize
C'est
\color none
 
\color inherit

\begin_inset Flex Alert
status open

\begin_layout Plain Layout
le temps d'exécution asymptotique
\end_layout

\end_inset


\color none
 qui est important.
\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
An outlook is always optional.
\end_layout

\end_inset


\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Itemize
Perspectives
\end_layout

\begin_deeper
\begin_layout Itemize
Le tri fusion est-il optimal ?
\end_layout

\begin_layout Itemize
Quelles autres contraintes 
\emph on
que
\emph default
 le temps existe-t'il ?
\end_layout

\end_deeper
\end_deeper
\end_body
\end_document
