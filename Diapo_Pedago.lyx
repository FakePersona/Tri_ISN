#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass beamer
\begin_preamble


\usepackage{graphicx}
\usepackage[french]{babel}
\end_preamble
\use_default_options false
\maintain_unincluded_children false
\language french
\language_package default
\inputencoding utf8
\fontencoding T1
\font_roman lmodern
\font_sans default
\font_typewriter lmodern
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 0
\use_package cancel 0
\use_package esint 1
\use_package mathdots 0
\use_package mathtools 0
\use_package mhchem 0
\use_package stackrel 0
\use_package stmaryrd 0
\use_package undertilde 0
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language french
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout FrameTitle
Notion de complexité
\end_layout

\begin_layout Block
\begin_inset Argument 2
status collapsed

\begin_layout Plain Layout
Comment comparer ces deux algorithmes?
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
<1> Première idée : mesurer le temps de calcul.
 
\end_layout

\begin_deeper
\begin_layout Itemize
<1> Problème : cette mesure dépendrait alors de l'ordinateur utilisé et
 non pas juste de l'algorithme.
 
\end_layout

\begin_layout Itemize
<2> Deuxième idée : compter les 
\shape italic
opérations élémentaires
\shape default
 effectuées.
 
\end_layout

\begin_layout Itemize
<2> Ici, on comptera par exemple le nombre de comparaisons effectuées.
 
\end_layout

\end_deeper
\end_deeper
\begin_layout Standard
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Frame
Images complexité tri_bulle.png 
\end_layout

\begin_layout Standard
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout FrameTitle
Analyse de complexité du tri bulle
\end_layout

\begin_layout Frame
Il faut au plus 
\begin_inset Formula $\frac{n^{2}}{2}-\frac{n}{2}$
\end_inset

 comparaisons, le terme le plus important est 
\begin_inset Formula $n^{2}$
\end_inset

, on parle de 
\shape italic
complexité quadratique
\shape default
.
\end_layout

\begin_layout Frame
Image graphe_quadratique.png
\end_layout

\begin_layout Standard
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout FrameTitle
Complexité du tri fusion
\end_layout

\begin_layout Frame
La complexité du tri fusion est en 
\begin_inset Formula $n\times ln(n)$
\end_inset

.
\end_layout

\begin_layout Frame
Image graphe_qlinéaire.png
\end_layout

\begin_layout Standard
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout FrameTitle
Comparaison des complexités.
\end_layout

\begin_layout Frame
La complexité du tri bulle dépasse vite celle du tri fusion.
\end_layout

\begin_layout Frame
Image Comparaison.png
\end_layout

\begin_layout Frame
\begin_inset ERT
status collapsed

\begin_layout Plain Layout

%lancer la simulation.
\end_layout

\end_inset


\end_layout

\end_body
\end_document
