#load "graphics.cma";;
#load "unix.cma";;

open Unix;;
open Graphics;;

close_graph();;
open_graph " 800x800";;

let minisleep (sec: float) =
    ignore (Unix.select [] [] [] sec);;

let n = 100 ;;
Random.init 42;;

let v = Array.make n 0;;

let init v = 
  for i = 0 to (n-1) do
	v.(i) <- (Random.int(80))
  done;;
      

let print_vect t =
  clear_graph ();
  let l = Array.length t in
  for i = 0 to (l-1) do
    fill_rect (i*800/l ) 0 (800 / l) (t.(i) * 5)
  done;;


let change_value i x t =
	let l = Array.length t in
	if x > t.(i) then begin
		set_color(black);
		fill_rect (i*800/l) (t.(i)*5 + 1) (800/l) ((x-t.(i))*5)
	end
	else begin
		set_color(white);
		fill_rect (i*800/l) (x*5 + 1) (800/l) ((t.(i) - x)*5)
	end;
	t.(i) <- x;;

let swap_values i j t =
  let s = t.(j) in
  change_value j (t.(i)) t; 
  change_value i s t;
  minisleep(0.05);;


  

let tri_bulle t =
  let l = Array.length t in
  for i = 0 to (l-1) do
    for j = 0 to (l-i-2) do
      if t.(j) > t.(j+1) then (swap_values j (j+1) t);
    done;
  done;;


let tri_fusion v =
	
	let n = Array.length v in
	let v' = Array.make n 0 in
	
	let sync d f =
		for k = d to (f-1) do
			v'.(k) <- v.(k)
		done in
	
	let fusion d m f =
		let i = ref(d)
		and j = ref(m) in
		for k = d to (f-1) do
		    if (!i >= m) then 
		      begin
			change_value k (v'.(!j)) v;
			j := !j + 1
		      end
		    else if (!j >= f) then 
		      begin
			change_value k (v'.(!i)) v;
			i := !i + 1
		      end
		    else if v'.(!i) < v'.(!j) then 
		      begin
			change_value k (v'.(!i)) v;
			i := !i + 1
		      end
		    else 
		      begin
			change_value k (v'.(!j)) v;
			j := !j + 1
		      end;
		    minisleep(0.05);
		done in
	
	let rec  tri_fusion' d f =
		if (f - d) > 1 then begin
			let m = (d + f)/2 in
			tri_fusion' d m;
			tri_fusion' m f;
			fusion d m f;
			sync d f;
		end in

	sync 0 n;
	tri_fusion' 0 n;;


let demo_bulle () =
  init v;
  print_vect v;
  tri_bulle v;;
  
let demo_fusion () =
  init v;
  print_vect v;
  tri_fusion v;;
